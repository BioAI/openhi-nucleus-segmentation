# OpenHI sub-module - nuclei segmentation
As part of [OpenHI](https://gitlab.com/BioAI/OpenHI/), this sub-module for nuclei segmentation is made to assist cellular annotation. 

![Cover image](img/cover.png)

## How to cite this work
Please cite OpenHI, see [How to cite OpenHI]()

And to cite particular part of this work, you can refer to the Zenodo DOI: [10.5281/zenodo.3598292](https://doi.org/10.5281/zenodo.3598292)

## Installation 
1. Install required packages: `pip install -r requirements.txt`

Noted that this code requires Python 3.6.x and was tested on Ubuntu 16 and 18. 

## Usage and documentation
This is a very simple project to run. Try running the main function of `main.py`. Everything else is contained in the same file. 

This part of the code is available in [OpenHI](https://gitlab.com/BioAI/OpenHI/) as well. So the updated version will be available there. 

## License
This code is part of [OpenHI](https://gitlab.com/BioAI/OpenHI/), thus, it is available under [GNU LGPL v3](LICENSE). 

