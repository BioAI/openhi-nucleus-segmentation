# -*- coding: utf-8 -*-
"""
Created on Mon Aug 19 16:04:05 2019

@author: Raytine

This script is a part of OpenHI (https://gitlab.com/BioAI/OpenHI). Written by Zeyu Gao and Haichuan Zhang. Documented by Pargorn Puttapirat
"""
import numpy as np
import cv2 as cv
import skimage.io as io
import skimage.transform as trans
from segmentation_models import Unet


class SegmentationModel(object):

    def __init__(self, model_path='model/unet_MoNuSeg.hdf5', target_size=(512, 512)):
        self.target_size = target_size
        # build model
        self.model = Unet('resnet34', input_shape=(512, 512, 3))
        # load weights
        self.model.load_weights(model_path, skip_mismatch=True, by_name=True)

    def predict(self, image_path):
        """This script accepts numpy.ndarray in RGB order (from io.imread), cv.imread is different and may cause a wrong prediction.

        :param str image_path: path to the image
        :return: 512-by-512 Numpy array
        :rtype: numpy.ndarray('float32')
        """
        image = io.imread(image_path)
        if image.shape[-1] > 3:
            image = image[:, :, :-1]
        image = trans.resize(image, self.target_size)
        image = image.reshape((1,) + image.shape)
        mask_pred = self.model.predict(image)
        mask_pred = mask_pred.reshape(mask_pred.shape[1:-1])
        return mask_pred


def watershed(mask, thresh=0.3):
    """Use image watershed to threshold independent nuclei from the U-Net prediction.

    :param numpy.ndarray mask: 512-by-512 Numpy array
    :param float thresh: control the sensitivity of seed
    :return: 512-by-512 Numpy array where 0 indicate background and -1 indicate boundaries
    :rtype: numpy.ndarray('float32')
    """
    mask = mask * 255
    mask = mask.astype('uint8')
    # gray\binary image
    gray = mask
    ret, binary = cv.threshold(gray, 0, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)
    # morphology operation
    kernel = cv.getStructuringElement(cv.MORPH_RECT, (3, 3))
    mb = cv.morphologyEx(binary, cv.MORPH_OPEN, kernel, iterations=2)
    sure_bg = cv.dilate(mb, kernel, iterations=3)
    # distance transform
    dist = cv.distanceTransform(mb, cv.DIST_L2, 3)
    ret, surface = cv.threshold(dist, dist.max() * thresh, 255, cv.THRESH_BINARY)
    surface_fg = np.uint8(surface)
    unknown = cv.subtract(sure_bg, surface_fg)
    ret, markers = cv.connectedComponents(surface_fg)
    # watershed transform
    markers += 1
    markers[unknown == 255] = 0
    mask = mask.reshape(mask.shape + (1,))
    mask = np.concatenate((mask, mask, mask), axis=2)
    markers = cv.watershed(mask, markers=markers)
    #        mask[markers == -1] = [0, 0, 255]
    return markers


def label2rgb(original_image, indexed_matrix):
    mask = np.zeros(original_image.shape)
    mask[indexed_matrix == -1] = tuple([0, 0, 0])

    return mask


if __name__ == '__main__':
    # Init the model
    segm = SegmentationModel()
    input_image = io.imread('image-input.png')
    pred_image = segm.predict('image-input.png')

    # Get boarder and save
    indexed_labeling_matrix = watershed(pred_image)
    np.savetxt('indexed_matrix.csv', indexed_labeling_matrix, fmt="%d", delimiter=",")

    # Save boarder
    final_image = label2rgb(input_image, indexed_labeling_matrix)
    cv.imwrite('image-output.png', final_image)

    # Fused border with the original image
    fused_image = input_image
    fused_image[indexed_labeling_matrix == -1] = tuple([255, 0, 0])
    cv.imwrite('image-blended.png', fused_image)

